import pymongo, os, datetime, requests, jsondiff
from flask import Flask, request
from flask_restplus import Api, swagger, Resource, Namespace
import json, redis, time

app = Flask(__name__)
api = Api(app, version='1.3', title="Weather Changes", description="Shows weather changes",
          contact_email='skmaifuj.alam@gslab.com')


@api.route('/GetNow/')
class GetNow(Resource):
    def get(self):
        t1 = time.time()
        v1 = requests.get('http://127.0.0.1:5000/a2')
        d1 = v1.json().get('Meters')
        print("Time Taken for API request",time.time() - t1)
        print("Size:",len(d1))
        return d1


@api.route('/GetFromCatch/')
class GetFromCatch(Resource):
    def get(self):
        t1=time.time()
        redisclient = redis.Redis()
        print(redisclient.dbsize())
        tkey=time.time()
        keys=redisclient.keys('*')
        print("Time to get all keys",time.time()-tkey)
        resp=[]
        for key in keys:
            val = redisclient.get(key)
            resp.append(json.loads(val))
        print("Time Taken from Cache",time.time()-t1)
        return resp


@api.route('/GetDifference/')
class GetDifference(Resource):
    def get(self):
        db=GetNow().get()
        redisclient = redis.Redis()
        ct = 0
        r4={}
        for i in db:
            db_id=i.get('MeterId')
            if redisclient.exists(db_id):
                #print(jsondiff.diff(i,redisclient.get(db_id),syntax='explicit'))
                db_value=i
                redis_value=redisclient.get(db_id)
                v1=json.loads(redis_value)
                res=jsondiff.diff(i,v1,syntax='symmetric')
                if res!={}:    # If there is no difference then take this value
                    print(i)
                    ct+=1
                    r4.update({db_id:db})
        print(r4)
        return r4


if __name__ == '__main__':
    app.run(debug=True, port=5500)
