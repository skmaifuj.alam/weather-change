import pymongo,os,datetime,requests,jsondiff
from flask import Flask, request
from flask_restplus import Api, swagger, Resource, Namespace
import json,redis,time
app = Flask(__name__)
#api = Api(app, version='1.3', title="Weather Changes", description="Shows weather changes", contact_email='skmaifuj.alam@gslab.com')

@app.route('/a1', methods=['GET'])
def get():
    print("in a1")
    client = pymongo.MongoClient()
    count=client['RateCard']['c1'].count_documents({})
    print(count)
    av=client['RateCard']['c1'].find({}, {"_id": False}).skip(count-3).limit(1)
    m1=av[0].get('Meters')
    #print(m1)
    t1=time.time()
    redisclient = redis.Redis()
    for i in m1:
        ids=i.get('MeterId')
        redisclient.set(ids,json.dumps(i))
    print(t1-time.time())
    print(av[0])
    return json.dumps(av[0])

@app.route('/a2', methods=['GET'])
def get2():
    print("in a2")
    client = pymongo.MongoClient()
    count = client['RateCard']['c1'].count_documents({})
    bv = client['RateCard']['c1'].find({}, {"_id": False}).skip(count - 1).limit(1)
    m2 = bv[0].get('Meters')
    return json.dumps(bv[0])


if __name__ == '__main__':
    app.run(debug=True,port=5000)