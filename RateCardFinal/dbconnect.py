import pymongo,os,datetime,requests,jsondiff
from flask import Flask, request
from flask_restplus import Api, swagger, Resource, Namespace
import marshmallow,jwt
app = Flask(__name__)
authorization={
    "apikey": {
        "type": "apiKey",
        "in": "header",
        "name": "X-API-KEY"
    }
}
# authorizations=authorization, security='apikey'
api = Api(app, version='1.3', title="Weather Changes", description="Shows weather changes", contact_email='skmaifuj.alam@gslab.com',
          )


@api.route('/Search_City/<string:City>')
class getCity(Resource):
    @api.doc(security='apikey',authorizations=authorization)
    def get(self,City):
        return City


if __name__ == '__main__':
    app.run(debug=True)

