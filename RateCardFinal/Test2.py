import pymongo, os, datetime, requests, jsondiff,redis,rejson
from flask import Flask, request
from rejson import path
from flask_restplus import Api, swagger, Resource, Namespace
import json,time


def dbg(x):
    print("Type: ", type(x))
    print("Value: ", x)


db = 'RateCard'
cn = 'c1'
client = pymongo.MongoClient()
count = client[db][cn].count_documents({})
print(count)
cr = client[db][cn].find({}, {'_id': False}).skip(count - 1)
re = cr[0]
meters = re.get('Meters')

tm1=time.time()

redisclient=redis.Redis()
v=0
print(redisclient)
vv={
            "EffectiveDate" : "2017-10-03T00:00:00Z",
            "IncludedQuantity" : 0.0,
            "MeterCategory" : "Azure Database for PostgreSQL",
            "MeterId" : "2374c11e-1b9a-4f60-8fe0-a9661c7665b4",
            "MeterName" : "Read Replica vCore",
            "MeterRates" : {
                "0" : 0.104
            },
            "MeterRegion" : "EU West",
            "MeterSubCategory" : "Storage Optimized - Compute Gen5",
            "MeterTags" : [],
            "Unit" : "10 Hour"
        }
v0={
            "EffectiveDate" : "2019-01-01T00:00:00Z",
            "IncludedQuantity" : 0.0,
            "MeterCategory" : "Virtual Machines",
            "MeterId" : "9db02507-6ab1-45fb-a0d7-c89da38ea908",
            "MeterName" : "E64 v3/E64s v3 Low Priority",
            "MeterRates" : {
                "0" : 2.928
            },
            "MeterRegion" : "EU West",
            "MeterSubCategory" : "Ev3/ESv3 Series Windows",
            "MeterTags" : [],
            "Unit" : "1 Hour"
        }
dbg(v0)
v1=json.dumps(vv)
v2=json.dumps(v0)
h=redisclient.set('ii',v1)
print(h)
print(redisclient.get('ii'))
rr=jsondiff.diff(json.loads(v1),json.loads(v2),syntax='explicit')
print(rr)