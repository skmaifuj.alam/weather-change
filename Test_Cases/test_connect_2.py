from App import mongo
import pymongo,mongomock,pytest,datetime
from mongomock import MongoClient
from unittest import mock
import mock

def test_connect_true():
    ob=mongo.MongoDB()
    a,b=ob.db_connect()
    client_test=mongomock.MongoClient(host=['localhost:27017'], document_class=dict, tz_aware=False, connect=True)
    assert a == client_test.HOST
    assert b == client_test.PORT

def test_get_time_stamp():
    ob = mongo.MongoDB()
    get_time=ob.get_time_stamp()
    get_time_test={'month': datetime.datetime.now().month, 'day':datetime.datetime.now().day , 'hour': datetime.datetime.now().hour, 'minute': datetime.datetime.now().minute}
    assert get_time == get_time_test

@mock.patch('azure.className.method',return_value="")
def test_insert(arg):
    ob = mongo.MongoDB()
    client_test=mongomock.MongoClient(host=['localhost:27017'], document_class=dict, tz_aware=False, connect=True)
    ins=client_test['weather']['Pune'].insert_one(data={'time':''})
    data = {}
    ob.insert(data)
    assert  ob.insert()== ins.acknowledged
