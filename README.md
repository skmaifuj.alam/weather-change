# weather-change

RateCardFinal folder have RateCard.py which have 3 GET flask-restplus endpoints.
1.GetNow to get data after hitting endpoint in json format.
2.GetFromCache to get all data(key-value) from redis server(localhost).
3.GetDiff to get difference from json data which we get from cache and database.

It contains MockServer.py which act as a server to provice to api end points one for providing data from db another for providing data from cache.
