import pymongo, os, datetime, requests, jsondiff,platform,json
from flask import Flask, request
from flask_restplus import Api, swagger, Resource, Namespace


app = Flask(__name__)
api = Api(app, version='1.3', title="Weather Changes", description="Shows weather changes",
          contact_email='skmaifuj.alam@gslab.com')


class MongoDB:
    def __init__(self):
        myhost = platform.system()
        if myhost == 'Windows':
            print("Working in localhost")
            self.host = 'localhost'
        else:
            print("Working in Container")
            self.host = 'mongoservice.project'

    def db_connect(self):
        self.client = pymongo.MongoClient(self.host)
        return self.client.HOST, self.client.PORT

    def get_time_stamp(self):
        hr = datetime.datetime.now().hour
        mn = datetime.datetime.now().minute
        d1 = datetime.datetime.now().day
        mo = datetime.datetime.now().month
        self.now_time = {'month': mo, 'day': d1, 'hour': hr, 'minute': mn}
        return self.now_time

    def insert(self, data):
        self.db_connect()
        print("Writing in DB")
        data.update({'time': self.get_time_stamp()})
        x2 = self.client['weather']['Pune'].insert_one(data)
        self.client.close()
        return x2.acknowledged

    def fetch_last(self):
        self.db_connect()
        count=self.client['weather']['Pune'].count_documents({})
        second_last = self.client['weather']['Pune'].find({}, {'_id': False}).skip(
            count - 2)  # Skipping to 2nd last
        self.client.close()
        for i in second_last:
            return i  # second_last have two document and we need only one so returning after getting one

    def dbCheck(self, timestamp):
        self.db_connect()
        search_result = self.client['weather']['Pune'].find(
            {'time.month': timestamp.get('month'),
             'time.day': timestamp.get('day'),
             'time.hour': timestamp.get('hour')},
            {'_id': False})  # Doing DB search
        self.client.close()
        for i in search_result:
            return i
        return {}  # If not found anything return empty Dict

    def weather_request(self):
        url = 'http://api.openweathermap.org/data/2.5/weather?q=Pune&appid=bed5975bf04e5540769c66d9af9e3873&units=metric'
        res = requests.get(url)
        self.data = res.json()
        return self.data  # Weather request result in dictionary


@api.route('/get_weather_now/')
class GetAll(Resource):
    ob = MongoDB()

    def get(self):
        now = self.ob.get_time_stamp()
        xx = self.ob.dbCheck(now)  # Checking in DB.If found in DB then return its all data
        if xx != {}:  # Found in db no need to request API
            print("Found in DB")
            return xx
        else:  # Request API and write in dbself.ob.GetAll.get(self)
            print("Not Found IN DB")
            requesting = self.ob.weather_request()  # Requesting API
            self.ob.insert(
                requesting)  # Storing result(which got from API Call) in DB as desire result is not found in db
            return self.ob.dbCheck(now)


@api.route('/recent_change/')
class RecentChange(Resource):
    ob = MongoDB()
    obgetall = GetAll()

    def get(self):
        now = GetAll.get(self)
        old = self.ob.fetch_last()
        print("Old", old)
        print("now", now)
        return jsondiff.diff(old, now,syntax='symmetric')


if __name__ == '__main__':
    app.run(debug=True, port=5001)
