import requests
import pymongo
import datetime
def weather_request():
    print("API Requesting ")
    url = 'http://api.openweathermap.org/data/2.5/weather?q=Pune&appid=bed5975bf04e5540769c66d9af9e3873&units=metric'
    res = requests.get(url)
    data = res.json()
    return data  # Weather request result in dictionary

def insert(data1):
    print("Writing in DB")
    client = pymongo.MongoClient('mongoservice.project', 27017)
    data1.update({'time': getTimeStamp()})
    x2 = client['weather']['Pune'].insert_one(data1)
    print("Write Status:",x2.acknowledged)
    client.close()

def getTimeStamp():
    hr = datetime.datetime.now().hour
    mn = datetime.datetime.now().minute
    d1 = datetime.datetime.now().day
    mo = datetime.datetime.now().month
    now_time = {'month': mo, 'day': d1, 'hour': hr, 'minute': mn}
    return now_time

def dbCheck(timestamp):
    client = pymongo.MongoClient('mongoservice.project', 27017)
    search_result=client['weather']['Pune'].find(
        {'time.month':timestamp.get('month'),
        'time.day': timestamp.get('day'),
        'time.hour': timestamp.get('hour')},
        {'_id': False})  # Doing DB search
    client.close()
    for i in search_result:
        print(i)
        return i
    return {}  #If not found anything return empty Dict


if __name__ == '__main__':
    print("Started Cron job at:",datetime.datetime.now())
    print("Checking in DB..")
    now = getTimeStamp()
    xx = dbCheck(now)
    if xx != {}:  # Found in db no need to request API
        print("Found in DB... closing cron job")
    else:  # Request API and write in db
        print("Not Found IN DB")
        requesting = weather_request()  # Requesting API
        insert(requesting)  # Storing result(which got from API Call) in DB as desire result is not found in db
